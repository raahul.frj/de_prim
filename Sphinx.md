# Sphinx Work Documentation


**Step 1:** Initially created a virtual environment to work with the project through anaconda by running 
```
python -m venv venv
```

![](/images/1.png "1.png")

**Step 2:** After reaching the project directory, activated the virtual environment by command. Then checked for the python version. 
```
source venv/scripts/activate
```
```
python --version
```

![](/images/2.png "2.png")

**Step 3:** Using **pip install** have installed the latest version of sphinx package.
```
pip install sphinx
```

![](/images/3.png "3.png")

**Step 4:** After installation of sphinx package, the sphinx package is started in order to work with sphix. After the quickstart command, created project as:

**Project Name:** GitClone
**Author(s):** Raahul
**Project Release:** 0.1

```
sphinx-quickstart
```

![](/images/4.png "4.png")

**Step 5:** After successful creation of project file in the directory, using the command, the document is produced into **HTML Files** in the file **docs/build**
```
sphinx-build -b html . docs/build
```

![](/images/5.png "5.png")

**Step 6:** After few changes with the html default template in config.py file, the below image shows the updated webpage created by sphinx



**Future Works:** Must work with the *sidebar, main pane, Tables, Indices, Contents* etc.

