# Python Coding Standards and Practises to be followed


## 1. Create a Code Repository and Implement Version Control
### Structure of the Python Project:
### Licence

This is in the root directory and is where you should add a licence for your project. Some licences are:
- GNU AGPLv3
- GNU GPLv3
- Mozilla Public License 2.0
- GNU LGPLv3
- Apache License 2.0
- MIT License
- The Unlicense

### README

- This is in the root directory too and is where you describe your project and what it does.
- You can write this in Markdown, reStructuredText, or plain text.

### Module Code

This holds your actual code that may be inside a subdirectory or inside root.

### requirements.txt

- This is not mandatory, but if you use this, you put it in the root directory.
- Here, you mention the modules and dependencies of the project- the things it will not run without.

### setup.py

This script in the root lets distutils build and distribute modules needed by the project.

### Documentation

Readable documentation is essential. This is placed in the docs directory.

### Tests

Most projects have tests- keep these in the tests directory.

## 2. Create Readable Documentation
- So, next in python best practices is readable documentation. You may find it burdensome, but it creates clean code.
- For this, you can use **Markdown, reStructuredText, Sphinx, or docstrings.**
- **Markdown and reStructuredText** are markup languages with plain text formatting syntax to make it **easier to mark up text and convert it to a format like HTML or PDF.**
- **reStructuredText** lets you create in-line documentation.
- **Sphinx** is a tool to easily create intelligent and beautiful documentation. It lets you **generate Python documentation from existing reStructuredText and export documentation in formats like HTML.**
- **Docstrings** are documentation strings at the **beginning of each module, class, or method.**

## 3. Follow Style Guidelines
- The **PEP8** holds some great community-generated proposals.
- **PEP stands for Python Enhancement Proposals- these are guidelines and standards for development.** This is to make sure all Python code looks and feels the same.
- One such guideline is to name classes with the **CapWords convention.**
- Use proper naming conventions for **variables, functions, methods, and more.**
- **Variables, functions, methods, packages, modules:** this_is_a_variable
- **Classes and exceptions:** CapWords
- **Protected methods and internal functions:** _single_leading_underscore
- **Private methods:** __double_leading_underscore
- **Constants:** CAPS_WITH_UNDERSCORES
- Use **4 spaces for indentation.** For more conventions, refer to PEP8.

## 4. Correct Broken Code Immediately
- Like with the broken code theory, **correct your broken code** immediately. If you let it be while you work on something else, it can lead to worse problems later.
- This is what Microsoft does. It once had a terrible production cycle with MS Word’s first version.
- So now, it follows a **‘zero defects methodology’**, and always corrects bugs and defects before proceeding.

## 5. Use the PyPI Instead of Doing it Yourself
- One of the reasons behind Python’s popularity is the **PyPI- this is the Python Package Index;** it has more than 198,190 projects at the time of writing.
- You should **use code from this** instead of writing it yourself- this **saves time and lets you focus** on the more important things.
- **Install these using pip.** You can also create and upload your own package here.

## 6. Use the Right Data Structures
Know the **benefits and limitations of different data structures** and choose the right one while Coding in Python.
 
## 7. Write Readable Code
- You should **use line breaks and indent** your code.
- Use **naming conventions for identifiers**- this makes it easier to understand the code.
- Use **comments, and whitespaces around operators and assignments.**
- Keep the **maximum line length 79 characters.**
 
## 8. Use Virtual Environments
- You should **create a virtual environment** for each project you create.
- This will **avoid any library clashes,** since different projects may need different versions of a certain library.
 
## 9. Write Object-Oriented Code
- Python is an **object-oriented language,** and everything in Python is an object. You **should use the object-oriented paradigm if writing code for Python.**
- This has the **advantages of data hiding and modularity. It allows reusability, modularity, polymorphism, data encapsulation, and inheritance.**
 
## 10. What Not to Do while Programming in Python
- **Avoid importing everything from a package**- this pollutes the global namespace and can cause clashes.
- Don’t implement best practices from other languages.
- Do not **turn off error reporting during development**- turn it off after it.
- **Don’t alter sys.path, use distutils** for that.

