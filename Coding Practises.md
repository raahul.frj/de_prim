# PEP8 Instructions for Python Coding Standards


## Indentation
### Use 4 spaces per indentation level.
Continuation lines should align wrapped elements either vertically using Python’s implicit line joining inside parentheses, brackets and braces, or using a hanging indent. 
<details>
  <summary markdown="span">*Other Instructions*</summary>
When using a hanging indent the following should be considered; there should be no arguments on the first line and further indentation should be used to clearly distinguish itself as a continuation line:
</details>

```
### Correct:
foo = long_function_name(var_one, var_two,
                     	var_three, var_four)

### Add 4 spaces (an extra level of indentation) to distinguish arguments from the rest.
def long_function_name(
    	var_one, var_two, var_three,
    	var_four):
	print(var_one)
```

```
### Hanging indents should add a level.
foo = long_function_name(
	var_one, var_two,
	var_three, var_four)

### Wrong:
### Arguments on first line forbidden when not using vertical alignment.

foo = long_function_name(var_one, var_two,
	var_three, var_four)

### Further indentation required as indentation is not distinguishable.
def long_function_name(
	var_one, var_two, var_three,
	var_four):
	print(var_one)
```

## The 4-space rule is optional for continuation lines.
Optional:

```
### Hanging indents *may* be indented to other than 4 spaces.
foo = long_function_name(
  var_one, var_two,
  var_three, var_four)
```
<details>
  <summary markdown="span">*Other Instructions*</summary>
When the conditional part of an if-statement is long enough to require that it be written across multiple lines, it’s worth noting that the combination of a two-character keyword (i.e. if), plus a single space, plus an opening parenthesis creates a natural 4-space indent for the subsequent lines of the multiline conditional. This can produce a visual conflict with the indented suite of code nested inside the if-statement, which would also naturally be indented to 4 spaces. This PEP takes no explicit position on how (or whether) to further visually distinguish such conditional lines from the nested suite inside the if-statement. Acceptable options in this situation include, but are not limited to:
</details>


```
### No extra indentation.
if (this_is_one_thing and
	that_is_another_thing):
	do_something()
 
### Add a comment, which will provide some distinction in editors
### supporting syntax highlighting.
if (this_is_one_thing and
	that_is_another_thing):
	# Since both conditions are true, we can frobnicate.
	do_something()
 
### Add some extra indentation on the conditional continuation line.
if (this_is_one_thing
    	and that_is_another_thing):
	do_something()
```

The closing brace/bracket/parenthesis on multiline constructs may either line up under the first non-whitespace character of the last line of list, as in:

``` 
my_list = [
	1, 2, 3,
	4, 5, 6,
	]
result = some_function_that_takes_arguments(
	'a', 'b', 'c',
	'd', 'e', 'f',
	)
```

or it may be lined up under the first character of the line that starts the multiline construct, as in:

```
my_list = [
	1, 2, 3,
	4, 5, 6,
]
result = some_function_that_takes_arguments(
	'a', 'b', 'c',
	'd', 'e', 'f',
)
```

## Tabs or Spaces?
- Spaces are the preferred indentation method.
- Tabs should be used solely to remain consistent with code that is already indented with tabs.
- Python disallows mixing tabs and spaces for indentation.

## Maximum Line Length
- Limit all lines to a maximum of 79 characters.
- For flowing long blocks of text with fewer structural restrictions (docstrings or comments), the line length should be limited to 72 characters.

<details>
  <summary markdown="span">*Other Instructions*</summary>
The preferred way of wrapping long lines is by using Python’s implied line continuation inside parentheses, brackets and braces. Long lines can be broken over multiple lines by wrapping expressions in parentheses. These should be used in preference to using a backslash for line continuation.
Backslashes may still be appropriate at times. For example, long, multiple with-statements could not use implicit continuation before Python 3.10, so backslashes were acceptable for that case:
</details> 

```
with open('/path/to/some/file/you/want/to/read') as file_1, \
 	open('/path/to/some/file/being/written', 'w') as file_2:
	file_2.write(file_1.read())
```

## Should a Line Break Before or After a Binary Operator?
For decades the recommended style was to break after binary operators. But this can hurt readability in two ways: the operators tend to get scattered across different columns on the screen, and each operator is moved away from its operand and onto the previous line.<br>

Here, the eye has to do extra work to tell which items are added and which are subtracted:

```
### Wrong:
### operators sit far away from their operands
income = (gross_wages +
      	taxable_interest +
      	(dividends - qualified_dividends) -
      	ira_deduction -
      	student_loan_interest) 
Following the tradition from mathematics usually results in more readable code:
 
### Correct:
### easy to match operators with operands
income = (gross_wages
      	+ taxable_interest
      	+ (dividends - qualified_dividends)
          - ira_deduction
      	- student_loan_interest)
```

In Python code, it is permissible to break before or after a binary operator, as long as the convention is consistent locally. 

## Blank Lines
- **Surround top-level function and class definitions with two blank lines.**

```
class MyFirstClass:
    pass


class MySecondClass:
    pass


def top_level_function():
    return None

```

- **Method definitions inside a class** are surrounded by a *single blank line*.

```
class MyClass:
    def first_method(self):
        return None

    def second_method(self):
        return None
```

- **Extra blank lines** may be used (sparingly) **to separate groups of related functions.** Blank lines may be omitted between a bunch of related one-liners (e.g. a set of dummy implementations).

```
def calculate_variance(number_list):
    sum_list = 0
    for number in number_list:
        sum_list = sum_list + number
    mean = sum_list / len(number_list)

    sum_squares = 0
    for number in number_list:
        sum_squares = sum_squares + number**2
    mean_squares = sum_squares / len(number_list)

    return mean_squares - mean**2
```

- **Use blank lines in functions, sparingly, to indicate logical sections.**

## Source File Encoding
- Code in the core Python distribution should always **use UTF-8, and should not have an encoding declaration.**
- In the standard library, **non-UTF-8 encodings should be used only for test purposes. Use non-ASCII characters sparingly, preferably only to denote places and human names.** If using non-ASCII characters as data, avoid noisy Unicode characters like z̯̯͡a̧͎̺l̡͓̫g̹̲o̡̼̘ and byte order marks.
- **All identifiers in the Python standard library MUST use ASCII-only identifiers, and SHOULD use English words wherever feasible (in many cases, abbreviations and technical terms are used which aren’t English).**
- Open source projects with a global audience are encouraged to adopt a similar policy.


Imports are always put at the top of the file, just after any module comments and docstrings, and before module globals and constants.

### Imports should be grouped in the following order:
1. **Standard library imports.**
2. **Related third party imports.**
3. **Local application/library specific imports.**

```
Imports
Imports should usually be on separate lines:
### Correct:
import os
import sys

### Wrong:
import sys, os
It’s okay to say this though:
 
### Correct:
from subprocess import Popen, PIPE
```

**You should put a blank line between each group of imports.**

**Wildcard imports (from <module> import *) should be avoided, as they make it unclear which names are present in the namespace, confusing both readers and many automated tools.**

## Module Level Dunder Names

Module level “dunders” (i.e. names with two leading and two trailing underscores) such as __all__, __author__, __version__, etc.

<details>
  <summary markdown="span">*Other Instructions*</summary>
Should be placed after the module docstring but before any import statements except from __future__ imports. Python mandates that future-imports must appear in the module before any other code except docstrings:
</details>

```
"""This is the example module.
This module does stuff.
"""
from __future__ import barry_as_FLUFL
 
__all__ = ['a', 'b', 'c']
__version__ = '0.1'
__author__ = 'Cardinal Biggles'
 
import os
import sys
```

## String Quotes
In Python, single-quoted strings and double-quoted strings are the same. 
<details>
  <summary markdown="span">*Other Instructions*</summary>
This PEP does not make a recommendation for this. Pick a rule and stick to it. When a string contains single or double quote characters, however, use the other one to avoid backslashes in the string. It improves readability.
For triple-quoted strings, always use double quote characters to be consistent with the docstring convention in PEP 257.
</details>

## Whitespace in Expressions and Statements

## Avoid extraneous whitespace in the following situations:
Immediately inside parentheses, brackets or braces:
``` 
### Correct:
spam(ham[1], {eggs: 2})

### Wrong:
spam( ham[ 1 ], { eggs: 2 } )
```
Between a trailing comma and a following close parenthesis:
```
### Correct:
foo = (0,)

### Wrong:
bar = (0, )
```
Immediately before a comma, semicolon, or colon:
```
### Correct:
if x == 4: print(x, y); x, y = y, x

### Wrong:
if x == 4 : print(x , y) ; x , y = y , x
```
However, in a slice the colon acts like a binary operator, and should have equal amounts on either side (treating it as the operator with the lowest priority).
<details>
  <summary markdown="span">*Other Instructions*</summary>
 In an extended slice, both colons must have the same amount of spacing applied. Exception: when a slice parameter is omitted, the space is omitted:
</details>

```
### Correct:
ham[1:9], ham[1:9:3], ham[:9:3], ham[1::3], ham[1:9:]

ham[lower:upper], ham[lower:upper:], ham[lower::step]

ham[lower+offset : upper+offset]

ham[: upper_fn(x) : step_fn(x)], ham[:: step_fn(x)]

ham[lower + offset : upper + offset]

### Wrong:
ham[lower + offset:upper + offset]

ham[1: 9], ham[1 :9], ham[1:9 :3]

ham[lower : : upper]

ham[ : upper]
```

Immediately before the open parenthesis that starts the argument list of a function call:
```
### Correct:
spam(1)

### Wrong:
spam (1)
```

Immediately before the open parenthesis that starts an indexing or slicing:
```
### Correct:
dct['key'] = lst[index]

### Wrong:
dct ['key'] = lst [index]
```
More than one space around an assignment (or other) operator to align it with another:
```
### Correct:
x = 1
y = 2
long_variable = 3

### Wrong:
x         	= 1
y         	= 2
long_variable = 3
```

## Other Recommendations
- **Avoid trailing whitespace anywhere.** Because it’s usually invisible, it can be confusing: e.g. **a backslash followed by a space and a newline does not count as a line continuation marker.** Some editors don’t preserve it and many projects (like CPython itself) have pre-commit hooks that reject it.
Always surround these binary operators **with a single space on either side: assignment (=), augmented assignment (+=, -= etc.), comparisons (==, <, >, !=, <>, <=, >=, in, not in, is, is not), Booleans (and, or, not).**

<details>
  <summary markdown="span">*Other Instructions*</summary>
If operators with different priorities are used, consider adding whitespace around the operators with the lowest priority(ies). Use your own judgement; however, never use more than one space, and always have the same amount of whitespace on both sides of a binary operator:
</details>

```
### Correct:
i = i + 1
submitted += 1
x = x*2 - 1
hypot2 = x*x + y*y
c = (a+b) * (a-b)

### Wrong:
i=i+1
submitted +=1
x = x * 2 - 1
hypot2 = x * x + y * y
c = (a + b) * (a - b)
```
Function annotations should use the normal rules for colons and always have spaces around the -> arrow if present. (See Function Annotations below for more about function annotations.):
```
### Correct:
def munge(input: AnyStr): ...
def munge() -> PosInt: ...

### Wrong:
def munge(input:AnyStr): ...
def munge()->PosInt: ...
```

Don’t use spaces around the = sign when used to indicate a keyword argument, or when used to indicate a default value for an unannotated function parameter:
```
### Correct:
def complex(real, imag=0.0):
	return magic(r=real, i=imag)

### Wrong:
def complex(real, imag = 0.0):
	return magic(r = real, i = imag)
```
When combining an argument annotation with a default value, however, do use spaces around the = sign:
```
### Correct:
def munge(sep: AnyStr = None): ...
def munge(input: AnyStr, sep: AnyStr = None, limit=1000): ...

### Wrong:
def munge(input: AnyStr=None): ...
def munge(input: AnyStr, limit = 1000): ...
```
Compound statements (multiple statements on the same line) are generally discouraged:
```
### Correct:
if foo == 'blah':
	do_blah_thing()
do_one()
do_two()
do_three()
Rather not:
 
### Wrong:
if foo == 'blah': do_blah_thing()
do_one(); do_two(); do_three()
```

While sometimes it’s okay to put an if/for/while with a small body on the same line, never do this for multi-clause statements. Also avoid folding such long lines!
Rather not:
```
### Wrong:
if foo == 'blah': do_blah_thing()
for x in lst: total += x
while t < 10: t = delay()
Definitely not:
 
### Wrong:
if foo == 'blah': do_blah_thing()
else: do_non_blah_thing()
 
try: something()
finally: cleanup() 
do_one(); do_two(); do_three(long, argument,
                         	list, like, this)
if foo == 'blah': one(); two(); three()
```
## When to Use Trailing Commas
Trailing commas are usually optional, except they are mandatory when making a tuple of one element. For clarity, it is recommended to surround the latter in (technically redundant) parentheses:
```
### Correct:
FILES = ('setup.cfg',)

### Wrong:
FILES = 'setup.cfg',

### Correct:
FILES = [
	'setup.cfg',
	'tox.ini',
	]
initialize(FILES,
       	error=True,
       	)

### Wrong:
FILES = ['setup.cfg', 'tox.ini',]
initialize(FILES, error=True,)
```
## Comments
- Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes!
- **Comments should be complete sentences. The first word should be capitalized, unless it is an identifier that begins with a lower case letter** (never alter the case of identifiers!).
- **Block comments generally consist of one or more paragraphs built out of complete sentences, with each sentence ending in a period.**
- You should use **two spaces after a sentence-ending period in multi- sentence comments,** except after the final sentence.
 
## Block Comments
- Block comments generally **apply to some (or all) code that follows them, and are indented to the same level as that code. Each line of a block comment starts with a # and a single space (unless it is indented text inside the comment).**

```
def quadratic(a, b, c, x):
    # Calculate the solution to a quadratic equation using the quadratic
    # formula.
    #
    # There are always two solutions to a quadratic equation, x_1 and x_2.
    x_1 = (- b+(b**2-4*a*c)**(1/2)) / (2*a)
    x_2 = (- b-(b**2-4*a*c)**(1/2)) / (2*a)
    return x_1, x_2
```

- **Paragraphs inside a block comment are separated by a line containing a single #.**

```
for i in range(0, 10):
    # Loop over i ten times and print out the value of i, followed by a
    # new line character
    print(i, '\n')
```
 
## Inline Comments
- Use inline comments sparingly.
- An **inline comment is a comment on the same line as a statement. Inline comments should be separated by at least two spaces from the statement. They should start with a # and a single space.**

```
x = 5  # This is an inline comment
```

- Inline comments are unnecessary and in fact distracting if they state the obvious. Don’t do this:
```
x = x + 1             	# Increment x
But sometimes, this is useful:
 
x = x + 1             	# Compensate for border
```

## Documentation Strings
- Conventions for **writing good documentation strings (a.k.a. “docstrings”) are immortalized in PEP 257.**
- **Write docstrings for all public modules, functions, classes, and methods.** Docstrings are not necessary for non-public methods, but you should have a comment that describes what the method does. This comment should appear after the def line.
- **PEP 257 describes good docstring conventions.** 

```
def quadratic(a, b, c, x):
    """Solve quadratic equation via the quadratic formula.

    A quadratic equation has the following form:
    ax**2 + bx + c = 0

    There always two solutions to a quadratic equation: x_1 & x_2.
    """
    x_1 = (- b+(b**2-4*a*c)**(1/2)) / (2*a)
    x_2 = (- b-(b**2-4*a*c)**(1/2)) / (2*a)

    return x_1, x_2
```

<details>
  <summary markdown="span">*Other Instructions*</summary>
Note that most importantly, the """ that ends a multiline docstring should be on a line by itself:


"""Return a foobang
Optional plotz says to frobnicate the bizbaz first.
"""
For one liner docstrings, please keep the closing """ on the same line:
"""Return an ex-parrot."""
</details>
 
 
## The following naming styles are commonly distinguished:
- b (single lowercase letter)
- B (single uppercase letter)
- lowercase
- lower_case_with_underscores

![](/images/6.png "6.png")

## Package and Module Names
- **Modules** should have **short, all-lowercase names.** Python packages should also have short, all-lowercase names.
 
## Class Names
-** should normally use the**CapWords convention.**
- The naming convention for functions may be used instead in cases where the interface is documented and used primarily as a callable.
 
## Function and Variable Names
**Function names** should be **lowercase**, with words separated by **underscores** as necessary to improve readability.
 
## Function and Method Arguments
- Always use **self for the first argument to instance methods**.
- Always use **cls for the first argument to class methods**.
 
## Method Names and Instance Variables
- Use the function naming rules: **lowercase with words separated by underscores** as necessary to improve readability.
- Use **one leading underscore only** for **non-public methods and instance variables**.

 
## Constants
Constants are usually defined on a module level and written in all **capital letters with underscores separating words**. Examples **include MAX_OVERFLOW and TOTAL.**

## Public and Internal Interfaces
- Even with __all__ set appropriately, **internal interfaces (packages, modules, classes, functions, attributes or other names) should still be prefixed with a single leading underscore.**

Use is not operator rather than not ... is. While both expressions are functionally identical, the former is more readable and preferred:
```
# Correct:
if foo is not None:
# Wrong:
if not foo is None:
```

**Always use a def statement instead of an assignment statement that binds a lambda expression directly to an identifier:**
```
# Correct:
def f(x): return 2*x

# Wrong:
f = lambda x: 2*x
try:
	import platform_specific_module
except ImportError:
	platform_specific_module = None

# Correct:
try:
	value = collection[key]
except KeyError:
	return key_not_found(key)
else:
	return handle_value(value)

# Wrong:
try:
	# Too broad!
	return handle_value(collection[key])
except KeyError:
	# Will also catch KeyError raised by handle_value()
	return key_not_found(key)
```

When a resource is local to a particular section of code, use a with statement to ensure it is cleaned up promptly and reliably after use. A try/finally statement is also acceptable.
```
# Correct:
with conn.begin_transaction():
	do_stuff_in_transaction(conn)

# Wrong:
with conn:
	do_stuff_in_transaction(conn)
```

The latter example doesn’t provide any information to indicate that the __enter__ and __exit__ methods are doing something other than closing the connection after a transaction. Being explicit is important in this case.

```
# Correct:
 
def foo(x):
	if x >= 0:
    	return math.sqrt(x)
	else:
    	return None
 
def bar(x):
	if x < 0:
    	return None
	return math.sqrt(x)

# Wrong:
 
def foo(x):
	if x >= 0:
    	return math.sqrt(x)
 
def bar(x):
	if x < 0:
    	return
	return math.sqrt(x)
```

**Use ''.startswith() and ''.endswith() instead of string slicing to check for prefixes or suffixes.
startswith() and endswith() are cleaner and less error prone:**
```
# Correct:
if foo.startswith('bar'):

# Wrong:
if foo[:3] == 'bar':
```
Object type comparisons should always use isinstance() instead of comparing types directly:
```
# Correct:
if isinstance(obj, int):

# Wrong:
if type(obj) is type(1):
```
For sequences, (strings, lists, tuples), use the fact that empty sequences are false:
```
# Correct:
if not seq:
if seq:

# Wrong:
if len(seq):
if not len(seq):
```
Don’t write string literals that rely on significant trailing whitespace. Such trailing whitespace is visually indistinguishable and some editors (or more recently, reindent.py) will trim them.
Don’t compare boolean values to True or False using ==:

```
# Correct:
if greeting:

# Wrong:
if greeting == True:
```
```
# Wrong:
if greeting is True:
```

Use of the flow control statements return/break/continue within the finally suite of a try...finally, where the flow control statement would jump outside the finally suite, is discouraged. 

```
# Wrong:
def foo():
	try:
    	1 / 0
	finally:
    	return 42
```
```
# Correct:
code: int
 class Point:
	coords: Tuple[int, int]
	label: str = '<unknown>'
```
```
# Wrong:
code:int  # No space after colon
code : int  # Space before colon
 class Test:
	result: int=0  # No spaces around equality sign
```

Luckily, there are tools that can help speed up this process. There are two classes of tools that you can use to enforce PEP 8 compliance: linters and autoformatters.

## Linters
Linters are programs that analyze code and flag errors. They provide suggestions on how to fix the error. Linters are particularly useful when installed as extensions to your text editor, as they flag errors and stylistic problems while you write. In this section, you’ll see an outline of how the linters work, with links to the text editor extensions at the end.

The best linters for Python code are the following:

- pycodestyle is a tool to check your Python code against some of the style conventions in PEP 8.


## Install pycodestyle using pip:

```
pip install pycodestyle

pycodestyle code.py
code.py:1:17: E231 missing whitespace after ','
code.py:2:21: E231 missing whitespace after ','
code.py:6:19: E711 comparison to None should be 'if cond is None:'

``` 
*flake8 is a tool that combines a debugger, pyflakes, with pycodestyle.*


## Install flake8 using pip:

```
pip install flake8

flake8 code.py
code.py:1:17: E231 missing whitespace after ','
code.py:2:21: E231 missing whitespace after ','
code.py:3:17: E999 SyntaxError: invalid syntax
code.py:6:19: E711 comparison to None should be 'if cond is None:'
```

*These are also available as extensions for Atom, Sublime Text, Visual Studio Code, and VIM.*

## Autoformatters
Autoformatters are programs that refactor your code to conform with PEP 8 automatically. Once such program is black, which autoformats code following most of the rules in PEP 8.

```
pip install black

for i in range(0,3):
    for j in range(0,3):
        if (i==2):
            print(i,j)

black code.py
reformatted code.py
All done! 

for i in range(0, 3):
    for j in range(0, 3):
        if i == 2:
            print(i, j)

```
